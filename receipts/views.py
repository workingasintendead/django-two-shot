from django.shortcuts import render, redirect, get_object_or_404
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from receipts.models import ExpenseCategory, Account, Receipt
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def show_receipt(request: HttpRequest) -> HttpResponse:
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt": receipt}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt: Receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create_receipt.html", context)


@login_required
def show_expensecategory(request: HttpRequest) -> HttpResponse:
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category": category}
    return render(request, "receipts/category_list.html", context)


@login_required
def show_account(request: HttpRequest) -> HttpResponse:
    account = Account.objects.filter(owner=request.user)
    context = {"account": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt: ExpenseCategory = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt: Account = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
